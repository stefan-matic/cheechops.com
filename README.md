# CheechOps

The website is available at https://cheechops.com

## Contributing

There is a dedicated [CONTRIBUTING](CONTRIBUTING.md) document.
You can ask for access if you wish to contribute.

## License

The project is under "DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE" so feel free to do what the fuck you want to.

## Pipeline

The pipeline consists of two stages:
  - build
  - deploy

The build phases tests that `docker compose` can be started successfully.

The deploy phase uses rsync to send the files to a remote server and initialize it via `docker compose`.
