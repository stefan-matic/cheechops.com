FROM nginx:latest

WORKDIR /app

COPY index.html /usr/share/nginx/html/index.html

COPY assets /usr/share/nginx/html/assets

CMD ["nginx", "-g", "daemon off;"]
